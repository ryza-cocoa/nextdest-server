package main

import (
	config "./internal/config"
	provider "./internal/dns/provider"
	httphelper "./internal/httphelper"
	nextdest "./internal/nextdest"
	"encoding/json"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"

	"net"
	"net/http"
	"os"
	"strings"
)

var cfg config.Config
var dnsProvider provider.Provider
// all available IPv6 addresses
var poolV6Addr *net.IPNet
var poolV6IP net.IP
// device name => stop func
var nextdestMgr map[string]func()

func init() {
	// command line args
	confPtr := flag.String("conf", "config.yml", "Path to the config file")

	// read config
	config.ReadConfig(&cfg, *confPtr)
	
	// output to stdout instead of the default stderr
	// can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	log.SetLevel(log.InfoLevel)
	logLevel := strings.ToLower(cfg.Server.LogLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warning":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.FatalLevel)
	default:
		log.Errorf("Unknown log level '%s', will set to Info level", logLevel)
		log.SetLevel(log.InfoLevel)
	}
	log.Debugln("cfg:", cfg)

	// init DNS provider
	providerName := strings.ToLower(cfg.DNS.Provider)
	switch providerName {
	case "cloudflare":
		dnsProvider = provider.NewCloudFlare(cfg.DNS.Configuration[providerName])
	default:
		log.Fatalln("DNS Provider '" + providerName + "' hasn't been implemented yet")
		os.Exit(-1)
	}

	// check available IPv6 address
	ip, block, err := net.ParseCIDR(cfg.Mapping.CIDR)
	if err != nil {
		log.Errorf("Parse error on %q: %v", cfg.Mapping.CIDR, err)
		return
	}
	poolV6IP = ip
	poolV6Addr = block

	nextdestMgr = make(map[string]func())
}

func main() {
	http.HandleFunc("/", httphelper.HttpIP)
	http.HandleFunc(cfg.Server.APIEndpoint, func(w http.ResponseWriter, req *http.Request) {
		errorMsg := ""
		dnsName := ""
		mode := ""
		devicePublicV6 := ""
		var portMapping []string
		var predefinedEntry config.DeviceConfig
		proceed := true

		reg, err := httphelper.RegisterIPv6(req, cfg.Server.Secret, cfg.Server.Tolerance)
		if err != nil {
			errorMsg = fmt.Sprintf("Error in register: %s", err.Error())
			log.Errorln(errorMsg)
		} else {
			log.Debugln("reg:", reg)
			cfgDefinedEntry, exists := cfg.Mapping.Name[reg.Name]
			predefinedEntry = cfgDefinedEntry
			if !exists {
				if !cfg.Mapping.PredefinedOnly {
					predefinedEntry = config.DeviceConfig{
						Mode:  cfg.Mapping.DefaultMode,
						DNS:   nextdest.RandomString(10) + "." + cfg.Server.Domain,
						Ports: nil,
					}
				} else {
					errorMsg = fmt.Sprintf("This device is not in predefined list, and server configuration doesn't allow exception")
					proceed = false
				}
			}
		}

		if proceed {
			devicePublicV6 = reg.Address
			mode = predefinedEntry.Mode
			portMapping = predefinedEntry.Ports

			prevStop, exists := nextdestMgr[reg.Name]
			if exists {
				log.Infof("Device %s has an active forwarding session, will stop that", reg.Name)
				prevStop()
			}

			var nextdestFunc func()(func(), error)
			if mode == "proxy" || mode == "hybrid" {
				if len(portMapping) == 0 {
					errorMsg = fmt.Sprintf("No port mapping defined for device %s", reg.Name)
					log.Warningf(errorMsg)
				}
				devicePublicV6Proxy, forwardFunc := nextdest.HandleForwarding(reg.Address, portMapping, poolV6IP, poolV6Addr, cfg.Mapping.Interface)
				devicePublicV6 = devicePublicV6Proxy.String()
				nextdestFunc = forwardFunc
			}

			log.Infof("Update DNS %s => %s", predefinedEntry.DNS, reg.Address)
			err = dnsProvider.UpdateDNS(&predefinedEntry, reg.Address)
			if err != nil {
				errorMsg = fmt.Sprintf("Error while updating DNS: %s", err.Error())
				log.Errorln(errorMsg)
			} else {
				dnsName = predefinedEntry.DNS
				if nextdestFunc != nil {
					stopFunc, err := nextdestFunc()
					if err != nil {
						errorMsg = fmt.Sprintf("Error while updating DNS: %s", err.Error())
						log.Errorln(errorMsg)
					}
					nextdestMgr[reg.Name] = stopFunc
				}
			}
		}

		w.Header().Set("Content-Type", "application/json")
		response := map[string]interface{}{
			"errors": errorMsg,
			"success": len(errorMsg) == 0,
			"dns_name": dnsName,
			"dns_addr": reg.Address,
			"direct_addr": devicePublicV6,
		}
		b, err := json.Marshal(response)
		_, _ = w.Write(b)
	})

	go func() {
		log.Infoln("Server listen on", cfg.Server.Listen)
		err := http.ListenAndServe(cfg.Server.Listen,nil)
		if err != nil {
			log.Fatalf("Cannot listen on %q", cfg.Server.Listen)
			os.Exit(-1)
		}
	}()

	p := make(chan struct{})
	<- p
}
