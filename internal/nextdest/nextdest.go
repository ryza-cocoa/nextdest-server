package NextDest

import (
	networkif "../interface"
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io"
	"math/rand"
	"net"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

type NextDestUsage struct {
	// 1: in use
	// 2: released
	mode int
	// created / released at time
	timestamp int64
}
var addressUsage map[string]NextDestUsage

func init()  {
	addressUsage = make(map[string]NextDestUsage)
}

type NextDest struct {
	src string
	dst string
	done chan int64
}

func NewNextDest(src, dst string) *NextDest {
	return &NextDest{
		src:  src,
		dst:  dst,
		done: make(chan int64),
	}
}

func HandleForwarding(v6 string, portMapping []string, ip net.IP, pool *net.IPNet, iface string) (*net.IP, func () (func(), error)) {
	log.Debugln("HandleForwarding", v6, portMapping)
	addressOnInterface := networkif.UpdateV6AddrsOnInterface(iface)

	log.Debugln("ip:", ip.String())
	log.Debugln("pool.Mask:", pool.Mask)

	var availableIPv6 net.IP
	for ip := ip.Mask(pool.Mask); pool.Contains(ip); {
		inc(ip)
		if !ip.IsGlobalUnicast() {
			continue
		}
		ipStr := ip.String()
		_, exists := addressOnInterface[ipStr]
		if !exists {
			usage, exists := addressUsage[ipStr]
			if exists {
				if usage.mode == 1 {
					// address is in use, skip
					continue
				} else if usage.mode == 2 {
					if time.Now().Unix() - usage.timestamp < 300 {
						// address released in less than 5 minutes
						// may encounter "bind: address already in use"
						log.Infof("skip %s, it was released in less than 5 minutes, may encounter \"bind: address already in use\"", ipStr)
						continue
					} else {
						availableIPv6 = ip
						break
					}
				}
			} else {
				availableIPv6 = ip
				break
			}
		}
	}

	if availableIPv6 == nil {
		log.Errorln("No public IPv6 available")
		return nil, nil
	}

	log.Debugln("will assign", availableIPv6.String())
	return &availableIPv6, func() (func(), error) {
		mappingIndex := 0
		mappingPort := make(map[uint16]uint16)
		for mappingIndex < len(portMapping) {
			mapping := portMapping[mappingIndex]
			ports := strings.Split(mapping, ":")
			switch len(ports) {
			case 1:
				asIs, err := toU16(ports[0])
				if err != nil { return nil, err }
				mappingPort[asIs] = asIs
			case 2:
				srcPort, err := toU16(ports[0])
				if err != nil { return nil, err }
				dstPort, err := toU16(ports[1])
				if err != nil { return nil, err }
				mappingPort[srcPort] = dstPort
			default:
				return nil, errors.New(fmt.Sprintf("Cannot understand port mapping %s", mapping))
			}
			mappingIndex++
		}

		devicePublicV6 := availableIPv6.String()
		err := assignReleaseIPv6ToInterface("add", devicePublicV6, iface)
		if err != nil { return nil, err }
		log.Debugln("Wait for 3 sec so that the required IPv6 can be set up")
		time.Sleep(3 * time.Second)
		addressUsage[devicePublicV6] = NextDestUsage{
			mode:      1,
			timestamp: time.Now().Unix(),
		}

		forwardWorkers := make([]*NextDest, len(mappingPort))
		mappingIndex = 0
		for srcPort, dstPort := range mappingPort {
			source := fmt.Sprintf("[%s]:%d", devicePublicV6, srcPort)
			dest := fmt.Sprintf("[%s]:%d", v6, dstPort)
			log.Infof("%s <=> %s", source, dest)
			next := NewNextDest(source, dest)
			err := next.Start()
			if err != nil {
				log.Errorln(err)
			}
			forwardWorkers[mappingIndex] = next
			mappingIndex++
		}
		return func() {
			log.Debugf("Workers for forwarding traffic between {} <=> {} will be stopped", devicePublicV6, v6)
			i := 0
			for i < len(forwardWorkers) {
				forwardWorkers[i].Stop()
				i++
			}
			log.Infof("Wait for 2 seconds to close all forwarding workers")
			time.Sleep(2 * time.Second)

			err := assignReleaseIPv6ToInterface("del", devicePublicV6, iface)
			log.Infof("Wait for 3 seconds to release IPv6 %s", devicePublicV6)
			time.Sleep(3 * time.Second)
			addressUsage[devicePublicV6] = NextDestUsage{
				mode:      2,
				timestamp: time.Now().Unix(),
			}

			if err != nil {
				log.Errorln(fmt.Sprintf("Error occurred while removing IPv6 %s from %s", devicePublicV6, iface))
			}
		}, nil
	}
}

func (n *NextDest) Start() error {
	log.Infoln(n.src, "to", n.dst)
	var listener, err = net.Listen("tcp6", n.src)
	if err != nil {
		return err
	}
	go n.run(listener)
	return nil
}

func (n *NextDest) run(l net.Listener) {
	for {
		select {
		case <-n.done:
			return
		default:
			dl := l.(*net.TCPListener)
			_ = dl.SetDeadline(time.Now().Add(time.Second * 1))
			src, err := l.Accept()
			if err != nil {
				errMsg := err.Error()
				if !strings.Contains(errMsg, "timeout") {
					log.Errorln("Cannot accept connection", errMsg)
				}
			} else {
				go n.handleConnection(src)
			}
		}
	}
}

func (n *NextDest) Stop() {
	if n.done == nil {
		return
	}
	n.done <- 1
	close(n.done)
	n.done = nil
}

func (n *NextDest) handleConnection(src net.Conn) {
	srcIp := src.RemoteAddr().String()
	log.Info("Accepted connection from ", srcIp)
	defer src.Close()

	dst, err := net.Dial("tcp6", n.dst)
	if err != nil {
		log.Error("Cannot connect to " + n.dst)
		return
	}
	defer dst.Close()
	wg := &sync.WaitGroup{}
	wg.Add(2)
	go n.copyConn(src, dst, wg)
	go n.copyConn(dst, src, wg)
	wg.Wait()
}

func (n* NextDest) copyConn(src, dst net.Conn, wg *sync.WaitGroup) {
	defer wg.Done()
	select {
	case <-n.done:
		return
	default:
		if _, err := io.Copy(dst, src); err != nil {
			log.Errorln("Cannot copy from ", src.RemoteAddr(), " to ", dst.RemoteAddr())
			n.Stop()
			return
		}
	}
}

func assignReleaseIPv6ToInterface(mode, ipv6, iface string) error {
	cmd := exec.Command("ip", "addr", mode, ipv6, "dev", iface)
	log.Infof("ip addr %s %s dev %s", mode, ipv6, iface)
	if err := cmd.Start(); err != nil {
		return err
	}

	if err := cmd.Wait(); err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			// The program has exited with an exit code != 0

			// This works on both Unix and Windows. Although package
			// syscall is generally platform dependent, WaitStatus is
			// defined for both Unix and Windows and in both cases has
			// an ExitStatus() method with the same signature.
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				log.Printf("Exit Status: %d", status.ExitStatus())
			}
		} else {
			log.Fatalf("cmd.Wait: %v", err)
		}
	}
	return nil
}

const charset = "abcdefghijklmnopqrstuvwxyz0123456789"

func stringWithCharset(length int, charset string) string {
	var seededRand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomString(length int) string {
	return stringWithCharset(length, charset)
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func toU16(str string) (uint16, error) {
	u16, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	if 0 < u16 && u16 < 65536 {
		castU16 := uint16(u16)
		return castU16, nil
	} else {
		return 0, errors.New(fmt.Sprintf("%d not in range (0, 65536)", u16))
	}
}
