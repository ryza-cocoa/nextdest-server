package Config

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"os"
)

type DeviceConfig struct{
	Mode string `yaml:"mode"`
	DNS string `yaml:"dns"`
	Ports []string `yaml:"ports"`
}

type Config struct {
	Server struct {
		Listen string `yaml:"listen"`
		Secret string `yaml:"secret"`
		APIEndpoint string `yaml:"api_endpoint"`
		Tolerance int64 `yaml:"tolerance"`
		Domain string `yaml:"domain"`
		LogLevel string `yaml:"log_level"`
	} `yaml:"server"`
	DNS struct {
		Provider string `yaml:"provider"`
		Configuration map[string]map[string]string `yaml:"configuration"`
	} `yaml:"dns"`
	Mapping struct {
		Interface string `yaml:"interface"`
		CIDR string `yaml:"cidr"`
		Name map[string]DeviceConfig `yaml:"name"`
		PredefinedOnly bool `yaml:"predefined_only"`
		DefaultMode string `yaml:"default_mode"`
	} `yaml:"mapping"`
}

func ReadConfig(cfg *Config, name string) {
	f, err := os.Open(name)
	if err != nil {
		processError(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
}

func processError(err error) {
	log.Errorln(err)
	os.Exit(2)
}
