package HttpHelper

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type RegisterV6 struct {
	Name  string `json:"name"`
	HMAC string `json:"hmac"`
	Address string `json:"addr"`
	Time string `json:"time"`
}

func HttpIP(w http.ResponseWriter, req *http.Request) {
	remoteIP := req.Header.Get("X-FORWARDED-FOR")
	if len(remoteIP) == 0 {
		remoteIP = req.RemoteAddr
	}
	remoteIP = strings.Split(remoteIP, "]")[0]
	remoteIP = strings.ReplaceAll(remoteIP, "[", "")

	_, _ = fmt.Fprintf(w, "%s\n", remoteIP)
	log.Infoln("Get IP", remoteIP)
}

func Base64Decode(message []byte) (b []byte, err error) {
	var l int
	b = make([]byte, base64.StdEncoding.DecodedLen(len(message)))
	l, err = base64.StdEncoding.Decode(b, message)
	if err != nil {
		return
	}
	return b[:l], nil
}

func RegisterIPv6(req *http.Request, secret string, maxDiff int64) (*RegisterV6, error) {
	log.Debugln("RegisterIPv6")
	bodyReader := bufio.NewReader(req.Body)
	bodyBytes, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		return nil, err
	}

	log.Debugln("Base64Decode")
	msg, err := Base64Decode(bodyBytes)
	if err != nil {
		return nil, err
	}

	// generate a new aes cipher using our 32 byte long key
	key := []byte(secret)
	hash := sha256.Sum256(key)
	log.Debugln("hash:", hash)
	c, err := aes.NewCipher(hash[:])
	if err != nil {
		return nil, err
	}

	log.Debugln("NewCBC")
	iv, msg := msg[:16], msg[16:]
	mode := cipher.NewCBCDecrypter(c, iv)
	mode.CryptBlocks(msg, msg)

	plainbody := string(msg)
	log.Debugln("plainbody:", plainbody)

	// Try to decode the request body into the struct. If there is an error,
	// respond to the client with the error message and a 400 status code.
	var registration RegisterV6
	err = json.NewDecoder(strings.NewReader(plainbody)).Decode(&registration)
	if err != nil {
		return nil, err
	}

	if maxDiff >= 0 {
		serverTime := time.Now().UTC().Unix()
		parsedTime, err := strconv.Atoi(registration.Time)
		if err != nil {
			return nil, err
		}
		clientTime := int64(parsedTime)

		if Abs(serverTime - clientTime) > maxDiff {
			return nil, errors.New(fmt.Sprintf("Server time %d, client time %d", serverTime, clientTime))
		}
	}

	h := hmac.New(sha256.New, []byte(secret))
	clientHMAC := registration.HMAC + ""
	registration.HMAC = secret
	jsonEncoded, err := json.Marshal(registration)
	// Write Data to it
	h.Write(jsonEncoded)

	// Get result and encode as hexadecimal string
	sha := hex.EncodeToString(h.Sum(nil))
	log.Debugln("sha:", sha)
	if sha != clientHMAC {
		return nil, errors.New("HMAC mismatched")
	}

	log.Infoln("Registration:", registration)
	return &registration, nil
}

func Abs(x int64) int64 {
	if x < 0 {
		return -x
	}
	return x
}