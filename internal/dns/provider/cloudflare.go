package Provider

import (
	config "../../config"
	"bytes"
	"encoding/json"
	"github.com/jpillora/go-tld"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

type CloudFlare struct {
	APIToken string
}

func NewCloudFlare(config map[string]string) *CloudFlare {
	return &CloudFlare{
		APIToken: "Bearer " + config["api_token"],
	}
}

func (c *CloudFlare)CreateDNS(cfg *config.DeviceConfig, v6Addr string, param map[string]interface{}) error {
	zoneID, exists := param["zoneID"].(string)
	if !exists {
		return errors.New("No 'zoneID' provided while creating")
	}

	name, exists := param["name"].(string)
	if !exists {
		return errors.New("No 'name' provided while creating")
	}

	proxied := false
	switch cfg.Mode {
	case "cdn":
		proxied = true
	case "hybrid":
		proxied = true
	}

	b, err := json.Marshal(map[string]interface{}{
		"type": "AAAA",
		"name": name,
		"content": v6Addr,
		"ttl": 1,
		"proxied": proxied,
	})
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://api.cloudflare.com/client/v4/zones/"+zoneID+"/dns_records", bytes.NewBuffer(b))
	req.Header.Add("Authorization", c.APIToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil { return err }

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { return err }

	var creationRet map[string]interface{}
	err = json.Unmarshal(body, &creationRet)
	if err != nil { return err }

	log.Debugln("creationRet:", creationRet)
	success := creationRet["success"].(bool)
	if !success {
		return errors.New(creationRet["messages"].(string))
	}
	return nil
}

func (c *CloudFlare) DeleteDNS(param map[string]interface{}) (*bool, error) {
	zoneID, exists := param["zoneID"].(string)
	if !exists {
		return nil, errors.New("No 'zoneID' provided while deleting")
	}

	recordID, exists := param["recordID"].(string)
	if !exists {
		return nil, errors.New("No 'recordID' provided while deleting")
	}

	client := &http.Client{}
	req, err := http.NewRequest("DELETE", "https://api.cloudflare.com/client/v4/zones/"+zoneID+"/dns_records/"+recordID, nil)
	req.Header.Add("Authorization", c.APIToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil { return nil, err }

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { return nil, err }

	var deleteRet map[string]interface{}
	err = json.Unmarshal(body, &deleteRet)
	if err != nil { return nil, err }

	success := deleteRet["success"].(bool)
	log.Debugln("deleteRet:", deleteRet)
	if !success {
		return &success, errors.New(deleteRet["messages"].(string))
	}
	return &success, nil
}

func (c *CloudFlare) QueryDNS(param map[string]interface{}) ([]interface{}, error) {
	zoneID, exists := param["zoneID"].(string)
	if !exists {
		return nil, errors.New("No 'zoneID' provided while querying")
	}

	name, exists := param["name"].(string)
	if !exists {
		return nil, errors.New("No 'name' provided while querying")
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.cloudflare.com/client/v4/zones/"+zoneID+"/dns_records?type=AAAA&name="+name, nil)
	req.Header.Add("Authorization", c.APIToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil { return nil, err }

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { return nil, err }

	var recordList map[string]interface{}
	err = json.Unmarshal(body, &recordList)
	if err != nil { return nil, err }

	result := recordList["result"].([]interface{})
	return result, nil
}

func (c *CloudFlare) UpdateDNS(cfg *config.DeviceConfig, v6Addr string) error {
	log.Debugln("update dns:", cfg)
	u, err := tld.Parse("https://" + cfg.DNS)
	if err != nil { return err }

	domain := u.Domain + "." + u.TLD
	name := u.Subdomain + "." + domain

	zoneId, err := c.queryZoneID(domain)
	if err != nil { return err }
	log.Infof("Domain %s, ZoneID %s", domain, *zoneId)

	param := map[string]interface{} {
		"name": name,
		"zoneID": *zoneId,
	}
	existedRecords, err := c.QueryDNS(param)
	if err != nil { return err }

	if len(existedRecords) != 0 {
		i := 0
		for i < len(existedRecords) {
			record := existedRecords[i].(map[string]interface{})
			recordID, exists := record["id"].(string)
			if exists {
				log.Debugln("record ID:", recordID)
				param = map[string]interface{}{
					"zoneID": *zoneId,
					"recordID": recordID,
				}
				_, err := c.DeleteDNS(param)
				if err != nil { return err }
			}
			i++
		}
	}

	param = map[string]interface{} {
		"name": name,
		"zoneID": *zoneId,
	}
	return c.CreateDNS(cfg, v6Addr, param)
}

func (c *CloudFlare) queryZoneID(domain string) (*string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.cloudflare.com/client/v4/zones?name="+domain, nil)
	req.Header.Add("Authorization", c.APIToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil { return nil, err }

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil { return nil, err }

	var zoneList map[string]interface{}
	err = json.Unmarshal(body, &zoneList)
	if err != nil { return nil, err }

	result := zoneList["result"].([]interface{})
	if len(result) != 1 {
		return nil, errors.New("No such domain '" + domain + "' found with provided API token")
	}
	zoneDetail := result[0].(map[string]interface{})
	zoneId, exists := zoneDetail["id"].(string)
	if !exists {
		return nil, errors.New("Zone returned, but cannot find zone id. Cloudflare API changed?")
	}
	return &zoneId, nil
}
