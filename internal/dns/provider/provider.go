package Provider

import config "../../config"

type DNSEntry struct {
	// DNS record name
	Name string
	// addr that the name resolves to
	Content string
}

type Provider interface {
	CreateDNS(cfg *config.DeviceConfig, v6Addr string, param map[string]interface{}) error
	QueryDNS(param map[string]interface{}) ([]interface{}, error)
	UpdateDNS(cfg *config.DeviceConfig, v6Addr string) error
	DeleteDNS(param map[string]interface{}) (*bool, error)
}
