package NetworkInterface

import (
	log "github.com/sirupsen/logrus"
	"net"
	"strings"
)

func UpdateV6AddrsOnInterface(iname string) map[string]bool {
	iface, err := net.InterfaceByName(iname)
	if err != nil {
		log.Errorln("Cannot get interface ", iface, ": ", err.Error())
		return nil
	}

	addrs, err := iface.Addrs()
	if err != nil {
		log.Errorln("Cannot get addresses on ", iface, ": ", err.Error())
		return nil
	}

	var addressOnInterface = make(map[string]bool)
	for _, a := range addrs {
		ip := a.String()
		if strings.Contains(ip, ":") && !strings.HasPrefix(ip, "fe80"){
			unicastIP, _, err := net.ParseCIDR(ip)
			if err != nil {
				log.Errorf("parse error on %q: %v", ip, err)
			}
			addressOnInterface[unicastIP.String()] = true
		}
	}
	return addressOnInterface
}
